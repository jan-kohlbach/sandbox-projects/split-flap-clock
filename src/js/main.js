const flip = (container, number) => {
  const currentNumber = container.dataset.number;
  const baseTop = container.querySelector('.base .top');
  const baseBottom = container.querySelector('.base .bottom');
  const flaps = container.querySelectorAll('.flap');
  const front = container.querySelector('.front');
  const back = container.querySelector('.back');

  container.dataset.number = number;
  front.dataset.content = currentNumber;
  back.dataset.content = number;

  baseTop.innerHTML = number;

  flaps.forEach((flap) => {flap.classList.add('show');});

  setTimeout(() => {
    flaps.forEach((flap) => {flap.classList.remove('show');});
    baseBottom.innerHTML = number;
  }, 600);
};

const set = (container, number) => {
  const base = container.querySelectorAll('.base div');

  container.dataset.number = number;
  base.forEach((div) => {div.innerHTML = number;});
};

const update = (key, time, shouldFlip) => {
  let currentTime = String(time);

  const container1 = document.querySelector(`.${key}-1`);
  const container2 = document.querySelector(`.${key}-2`);

  if (currentTime.length === 1) currentTime = `0${time}`;

  const number1 = currentTime.substr(0, 1);
  const number2 = currentTime.substr(1, 1);

  if (container1.dataset.number !== number1) {
    shouldFlip
      ? flip(container1, number1)
      : set(container1, number1);
  }

  if (container2.dataset.number !== number2) {
    shouldFlip
      ? flip(container2, number2)
      : set(container2, number2);
  }
};

const setTime = (shouldFlip) => {
  const date = new Date();

  update('hour', date.getHours(), shouldFlip);
  update('minute', date.getMinutes(), shouldFlip);
  update('second', date.getSeconds(), shouldFlip);
};

setTime(false);

setInterval(() => {
  setTime(true);
}, 1000);
